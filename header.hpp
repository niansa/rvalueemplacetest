#ifndef _HEADER_HPP
#define _HEADER_HPP
#include <string>
#include <vector>



class Embed {
public:
    std::string title, description, image, thumbnail;

    Embed() {}

    Embed& set_title(const std::string&);
    Embed& set_description(const std::string&);
    Embed& set_image(const std::string&);
    Embed& set_thumbnail(const std::string&);
};



class MessageByConstRef {
public:
    std::vector<Embed> embeds;

    MessageByConstRef() {}
    MessageByConstRef& add_embed(const Embed&);
};


class MessageByRValue {
public:
    std::vector<Embed> embeds;

    MessageByRValue() {}
    MessageByRValue& add_embed(Embed&&);
};


class MessageByLValue {
public:
    std::vector<Embed> embeds;

    MessageByLValue() {}
    MessageByLValue& add_embed(Embed);
};

// Prevents the compiler from detecting the final producs as unused which way it may skip creating it in first place
void anti_optimizer_function(const Embed&);
void anti_optimizer_function(const MessageByConstRef&);
void anti_optimizer_function(const MessageByRValue&);
void anti_optimizer_function(const MessageByLValue&);
#endif // _HEADER_HPP
