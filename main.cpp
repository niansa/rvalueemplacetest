#include "header.hpp"
#include "Timer.hpp"

#include <iostream>
#include <chrono>
#include <array>



inline
Embed create_embed() {
    Embed embed;
    embed.set_title("This is a long title to prevent optimization into on-stack value")
         .set_description("This is a long description to prevent optimization into on-stack value")
         .set_image("https://This.is/a/long/image/URL/to/prevent/optimization/into/on-stack/value")
         .set_thumbnail("https://This.is/a/long/thumbnail/URL/to/prevent/optimization/into/on-stack/value");
    return embed;
}

int main() {
    // Initial checks
    {
        MessageByConstRef msg;
        auto embed = create_embed();
        auto title_ptr = embed.title.data();
        msg.add_embed(std::move(embed));
        std::cout << "ByConstRef works: " << (title_ptr != msg.embeds[0].title.data()) << std::endl;
    }
    {
        MessageByLValue msg;
        auto embed = create_embed();
        auto title_ptr = embed.title.data();
        msg.add_embed(std::move(embed));
        std::cout << "ByLValue works: " << (title_ptr == msg.embeds[0].title.data()) << std::endl;
    }
    {
        MessageByRValue msg;
        auto embed = create_embed();
        auto title_ptr = embed.title.data();
        msg.add_embed(std::move(embed));
        std::cout << "ByRValue works: " << (title_ptr == msg.embeds[0].title.data()) << std::endl;
    }

    // Settings
    constexpr unsigned repeats = 1000000,
                       main_repeats = 8,
                       heatups = 2;
    std::cout << "\nEach test in set is repeated " << repeats << " times and the whole set is repeated " << main_repeats << " times plus " << heatups << " times heatup.\n" << std::endl;

    // Collection of results
    std::array<unsigned long, 3> results = {0, 0, 0};
    unsigned prepare_time = 0;

    // Measure a couple of times
    unsigned long duration;
    Timer timer;
    Embed embed;
    for (unsigned i = 0; i != main_repeats+heatups; i++) {
        // Preparation time test
        std::cout << "Running preparation time test" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            embed = create_embed();
            anti_optimizer_function(embed);
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (i < heatups) {
            prepare_time += duration;
        }

        // Test #1
        std::cout << "Running test #1: By ByConstRef" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            embed = create_embed();
            MessageByConstRef msg;
            msg.add_embed(std::move(embed));
            anti_optimizer_function(msg);
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (i < heatups) {
            results[0] += duration;
        }

        // Test #2
        std::cout << "Running test #2: By ByLValue" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            embed = create_embed();
            MessageByLValue msg;
            msg.add_embed(std::move(embed));
            anti_optimizer_function(msg);
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (i < heatups) {
            results[1] += duration;
        }

        // Test #3
        std::cout << "Running test #3: By ByRValue" << std::endl;
        timer.reset();
        for (unsigned i = 0; i != repeats; i++) {
            embed = create_embed();
            MessageByRValue msg;
            msg.add_embed(std::move(embed));
            anti_optimizer_function(msg);
        }
        duration = timer.get<std::chrono::milliseconds>();
        std::cout << "Time spent: " << duration << "ms" << std::endl;
        if (i < heatups) {
            results[2] += duration;
        }
    }

    // Print final results calculating average
    prepare_time /= main_repeats;
    std::cout << "\nTests have finished.\n"
                 "Average time spent by ByConstRef: " << (results[0]/main_repeats)-prepare_time << "ms\n"
                 "Average time spent by ByLValue: " << (results[1]/main_repeats)-prepare_time << "ms\n"
                 "Average time spent by ByRValue: " << (results[2]/main_repeats)-prepare_time << "ms\n";
}
