#include "header.hpp"


Embed& Embed::set_title(const std::string& _title) {
    title = _title;
    return *this;
}
Embed& Embed::set_description(const std::string& _description) {
    description = _description;
    return *this;
}
Embed& Embed::set_image(const std::string& _image) {
    image = _image;
    return *this;
}
Embed& Embed::set_thumbnail(const std::string& _thumbnail) {
    thumbnail = _thumbnail;
    return *this;
}

MessageByConstRef& MessageByConstRef::add_embed(const Embed& embed) {
    embeds.emplace_back(embed);
    return *this;
}
MessageByRValue& MessageByRValue::add_embed(Embed&& embed) {
    embeds.emplace_back(std::move(embed));
    return *this;
}
MessageByLValue& MessageByLValue::add_embed(Embed embed) {
    embeds.emplace_back(std::move(embed));
    return *this;
}


void anti_optimizer_function(const Embed&) {}
void anti_optimizer_function(const MessageByConstRef&) {}
void anti_optimizer_function(const MessageByRValue&) {}
void anti_optimizer_function(const MessageByLValue&) {}
